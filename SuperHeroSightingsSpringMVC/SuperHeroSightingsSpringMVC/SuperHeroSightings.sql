drop database if exists SuperHeroSightings;

create database SuperHeroSightings;

use SuperHeroSightings;

create table SuperHVs
(SuperHVId int primary key auto_increment,
Name varchar(30) not null,
Description varchar(45) null);

create table Powers
(SuperPowerId int primary key auto_increment,
Name varchar(30) not null,
Description varchar(45) null);

create table SuperPowers
(SuperHVId int not null,
SuperPowerId int not null,
primary key (SuperHVId, SuperPowerId),
foreign key (SuperHVId) references SuperHVs (SuperHVId),
foreign key (SuperPowerId) references Powers (SuperPowerId));

create table Orgs
(OrgId int primary key auto_increment,
Name varchar(30) not null,
Description varchar(45) null,
Address varchar(45) null,
PhoneNumber varchar(15) null); 

create table SuperOrgs
(SuperHVId int not null,
OrgId int not null,
primary key (SuperHVId, OrgId),
foreign key (SuperHVId) references SuperHVs (SuperHVId),
foreign key (OrgId) references Orgs (OrgId));

create table Locations
(LocaId int primary key auto_increment,
Name varchar(30) not null,
Description varchar(45) null,
Address varchar(45) not null,
Latitude double not null,
Longitude double not null);

create table Sightings
(SightingId int primary key auto_increment,
Date date not null,
SuperHVId int not null,
LocaId int not null,
foreign key (SuperHVId) references SuperHVs (SuperHVId),
foreign key (LocaId) references Locations (LocaId));   