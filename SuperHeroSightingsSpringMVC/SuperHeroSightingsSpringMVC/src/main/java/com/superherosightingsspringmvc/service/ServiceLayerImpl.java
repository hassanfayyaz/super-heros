package com.superherosightingsspringmvc.service;

import com.superherosightingsspringmvc.dao.SuperHeroSightingsDao;
import com.superherosightingsspringmvc.dto.Location;
import com.superherosightingsspringmvc.dto.Org;
import com.superherosightingsspringmvc.dto.SuperPower;
import com.superherosightingsspringmvc.dto.SuperSighting;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import java.util.List;
import javax.inject.Inject;
import org.springframework.stereotype.Service;

/**
 *
 * @author brian russick
 */
@Service
public class ServiceLayerImpl implements ServiceLayer {
    
    SuperHeroSightingsDao dao;
    
    @Inject
    public ServiceLayerImpl(SuperHeroSightingsDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Location> getLocationAll() {
        return dao.getLocationAll();
    }

    @Override
    public List<SuperPower> getSuperPowerAll() {
        return dao.getSuperPowerAll();
    }

    @Override
    public List<Org> getOrgAll() {
        return dao.getOrgAll();
    }

    @Override
    public List<SuperHeroSuperVill> getSuperHeroSuperVillAll() {
        return dao.getSuperHeroSuperVillAll();
    }

    @Override
    public List<SuperSighting> getSuperSightingsAll() {
        return dao.getSuperSightingsAll();
    }

    @Override
    public void addLocation(Location loca) {
        dao.addLocation(loca);
    }

    @Override
    public void addSuperPower(SuperPower superP) {
        dao.addSuperPower(superP);
    }

    @Override
    public void addOrg(Org org) {
        dao.addOrg(org);
    }

    @Override
    public void addSuperHeroSuperVill(SuperHeroSuperVill superHV) {
        dao.addSuper(superHV);
    }

    @Override
    public void addSuperSighting(SuperSighting sighting) {
        dao.addSuperSighting(sighting);
    }

    @Override
    public Location getLocation(int locaId) {
        return dao.getLocation(locaId);
    }

    @Override
    public Org getOrg(int orgId) {
        return dao.getOrg(orgId);
    }

    @Override
    public SuperPower getSuperPower(int superPowerId) {
        return dao.getSuperPower(superPowerId);
    }

    @Override
    public SuperHeroSuperVill getSuperHeroSuperVill(int superHVId) {
        return dao.getSuperHeroSuperVill(superHVId);
    }

    @Override
    public SuperSighting getSuperSighting(int superSightingId) {
        return dao.getSuperSighting(superSightingId);
    }

    @Override
    public void removeLocation(int locaId) {
        dao.removeLocation(locaId);
    }

    @Override
    public void removeOrg(int orgId) {
        dao.removeOrg(orgId);
    }

    @Override
    public void removeSuperPower(int superPowerId) {
        dao.removeSuperPower(superPowerId);
    }

    @Override
    public void removeSuperHeroSuperVill(int superHVId) {
        dao.removeSuperHeroSuperVill(superHVId);
    }

    @Override
    public void removeSuperSighting(int superSightingId) {
        dao.removeSuperSighting(superSightingId);
    }

    @Override
    public void updateLocation(Location loca) {
        dao.updateLocation(loca);
    }

    @Override
    public void updateOrg(Org org) {
        dao.updateOrg(org);
    }

    @Override
    public void updateSuperPower(SuperPower superP) {
        dao.updateSuperPower(superP);
    }

    @Override
    public void updateSuperHeroSuperVill(SuperHeroSuperVill superHV) {
        dao.updateSuperHeroSuperVill(superHV);
    }

    @Override
    public void updateSuperSighting(SuperSighting sighting) {
        dao.updateSuperSighting(sighting);
    }

    @Override
    public List<SuperSighting> getNewSuperSighting() {
        return dao.getNewSuperSighting();
    }  
}