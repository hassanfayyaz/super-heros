package com.superherosightingsspringmvc.service;

import com.superherosightingsspringmvc.dto.Location;
import com.superherosightingsspringmvc.dto.Org;
import com.superherosightingsspringmvc.dto.SuperPower;
import com.superherosightingsspringmvc.dto.SuperSighting;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import java.util.List;

/**
 *
 * @author brian russick
 */
public interface ServiceLayer {
    
    public List<Location> getLocationAll();
    
    public List<SuperPower> getSuperPowerAll();
    
    public List<Org> getOrgAll();
    
    public List<SuperHeroSuperVill> getSuperHeroSuperVillAll();
    
    public List<SuperSighting> getSuperSightingsAll();

    public void addLocation(Location loca);
    
    public void addSuperPower(SuperPower superP);
    
    public void addOrg(Org org);
    
    public void addSuperHeroSuperVill(SuperHeroSuperVill superHV);
    
    public void addSuperSighting(SuperSighting sighting);

    public Location getLocation(int locaId);
    
    public Org getOrg(int orgId);
    
    public SuperPower getSuperPower(int superPowerId);
    
    public SuperHeroSuperVill getSuperHeroSuperVill(int superHVId);
    
    public SuperSighting getSuperSighting(int superSightingId);

    public void removeLocation(int locaId);
    
    public void removeOrg(int orgId);
    
    public void removeSuperPower(int superPowerId);
    
    public void removeSuperHeroSuperVill(int superHVId);
    
    public void removeSuperSighting(int superSightingId);

    public void updateLocation(Location loca);
    
    public void updateOrg(Org org);
    
    public void updateSuperPower(SuperPower superP);
    
    public void updateSuperHeroSuperVill(SuperHeroSuperVill superHV);
    
    public void updateSuperSighting(SuperSighting sighting);

    public List<SuperSighting> getNewSuperSighting();   
}