package com.superherosightingsspringmvc.controller;

import com.superherosightingsspringmvc.dto.Org;
import com.superherosightingsspringmvc.dto.SuperPower;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import com.superherosightingsspringmvc.service.ServiceLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author brian russick
 */
@Controller
public class SuperController {
    
    ServiceLayer service;

    @Inject
    public SuperController(ServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/supers", method = RequestMethod.GET)
    public String displaySupersPage(Model model) {

        List<SuperHeroSuperVill> superList = service.getSuperHeroSuperVillAll();
        
        List<Org> organizationList = service.getOrgAll();
        
        List<SuperPower> powerList = service.getSuperPowerAll();

        model.addAttribute("superList", superList);
        
        model.addAttribute("organizationList", organizationList);
        
        model.addAttribute("powerList", powerList);

        return "supers";
    }

    @RequestMapping(value = "/createSuper", method = RequestMethod.POST)
    public String createSuper(HttpServletRequest request, Model model, String[] orgIdList, String[] superPowerIdList) {
        
      SuperHeroSuperVill superPerson = new SuperHeroSuperVill();
      superPerson.setName(request.getParameter("name"));
      superPerson.setDescription(request.getParameter("description")); 
      
      List<Integer> orgIdList2 = new ArrayList<>();
      
      if (orgIdList != null) {
        for (String currentOrganizationId : orgIdList) {
            int currentId = Integer.parseInt(currentOrganizationId);
            orgIdList2.add(currentId);
        }
      }
      superPerson.setOrgIdList(orgIdList2);
      
      List<Integer> superPowerIdList2 = new ArrayList<>();
      
      if (superPowerIdList != null) {
        for (String currentPowerId : superPowerIdList) {
            int currentId = Integer.parseInt(currentPowerId);
            superPowerIdList2.add(currentId);
        }
      }
      superPerson.setSuperPowerIdList(superPowerIdList2);

      service.addSuperHeroSuperVill(superPerson);

      return "redirect:supers";
      
    }

    @RequestMapping(value = "/superDetails", method = RequestMethod.GET)
    public String displaySuperDetails(HttpServletRequest request, Model model) {
        String superIdParameter = request.getParameter("superId");

        int superId = Integer.parseInt(superIdParameter);

        SuperHeroSuperVill superPerson = service.getSuperHeroSuperVill(superId);
        
        List<Integer> orgIdList = superPerson.getOrgIdList();
        
        List<Org> organizationList = new ArrayList<>();
        
        for (int currentId : orgIdList) {
            Org org = service.getOrg(currentId);
            organizationList.add(org);
        }
        
        List<Integer> superPowerIdList = superPerson.getSuperPowerIdList();
        
        List<SuperPower> powerList = new ArrayList<>();
        
        for (int currentId : superPowerIdList) {
            SuperPower power = service.getSuperPower(currentId);
            powerList.add(power);
        }

        model.addAttribute("superPerson", superPerson);
        
        model.addAttribute("organizationList", organizationList);
        
        model.addAttribute("powerList", powerList);

        return "superDetails";
    }

    @RequestMapping(value = "/deleteSuper", method = RequestMethod.GET)
    public String deleteSuper(HttpServletRequest request) {
        String superIdParameter = request.getParameter("superId");

        int superId = Integer.parseInt(superIdParameter);

        service.removeSuperHeroSuperVill(superId);

        return "redirect:supers";
    }

    @RequestMapping(value = "/displayEditSuper", method = RequestMethod.GET)
    public String displayEditSuper(HttpServletRequest request, Model model) {
        String superIdParameter = request.getParameter("superId");

        int superId = Integer.parseInt(superIdParameter);

        SuperHeroSuperVill superPerson = service.getSuperHeroSuperVill(superId);
        
        List<Org> organizationList = service.getOrgAll();
        
        List<SuperPower> powerList = service.getSuperPowerAll();

        model.addAttribute("super", superPerson);
        
        model.addAttribute("organizationList", organizationList);
        
        model.addAttribute("powerList", powerList);

        return "editSuper";
    }

    @RequestMapping(value = "/editSuper", method = RequestMethod.POST)
    public String editSuper(@ModelAttribute("super") SuperHeroSuperVill superPerson) {
        service.updateSuperHeroSuperVill(superPerson);

        return "redirect:supers";
    }   
}