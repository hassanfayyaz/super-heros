package com.superherosightingsspringmvc.controller;

import com.superherosightingsspringmvc.dto.SuperPower;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import com.superherosightingsspringmvc.service.ServiceLayer;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author brian russick
 */
@Controller
public class PowerController {
    
   ServiceLayer service;

    @Inject
    public PowerController(ServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/powers", method = RequestMethod.GET)
    public String displayPowersPage(Model model) {

        List<SuperPower> powerList = service.getSuperPowerAll();
        
        List<SuperHeroSuperVill> superList = service.getSuperHeroSuperVillAll();

        model.addAttribute("powerList", powerList);
        
        model.addAttribute("superList", superList);

        return "powers";
    }

    @RequestMapping(value = "/createPower", method = RequestMethod.POST)
    public String createPower(HttpServletRequest request, Model model, String[] superHVIdList) {
        
      SuperPower power = new SuperPower();
      power.setName(request.getParameter("name"));
      power.setDescription(request.getParameter("description"));
      
      List<Integer> superHVIdList2 = new ArrayList<>();
      
      if (superHVIdList != null) {
        for (String currentSuperId : superHVIdList) {
            int currentId = Integer.parseInt(currentSuperId);
            superHVIdList2.add(currentId);
        }
      }
      power.setSuperHVIdList(superHVIdList2);

      service.addSuperPower(power);

      return "redirect:powers";
      
    }

    @RequestMapping(value = "/powerDetails", method = RequestMethod.GET)
    public String displayPowerDetails(HttpServletRequest request, Model model) {
        String powerIdParameter = request.getParameter("powerId");

        int powerId = Integer.parseInt(powerIdParameter);

        SuperPower power = service.getSuperPower(powerId);
        
        List<Integer> superHVIdList = power.getSuperHVIdList();
        
        List<SuperHeroSuperVill> superList = new ArrayList<>();
        
        for (int currentId : superHVIdList) {
            SuperHeroSuperVill superPerson = service.getSuperHeroSuperVill(currentId);
            superList.add(superPerson);
        }

        model.addAttribute("power", power);
        
        model.addAttribute("superList", superList);

        return "powerDetails";
    }

    @RequestMapping(value = "/deletePower", method = RequestMethod.GET)
    public String deletePower(HttpServletRequest request) {
        String powerIdParameter = request.getParameter("powerId");

        int powerId = Integer.parseInt(powerIdParameter);

        service.removeSuperPower(powerId);

        return "redirect:powers";
    }

    @RequestMapping(value = "/displayEditPower", method = RequestMethod.GET)
    public String displayEditPower(HttpServletRequest request, Model model) {
        String powerIdParameter = request.getParameter("powerId");

        int powerId = Integer.parseInt(powerIdParameter);

        SuperPower power = service.getSuperPower(powerId);
        
        List<SuperHeroSuperVill> superList = service.getSuperHeroSuperVillAll();
        
        model.addAttribute("superList", superList);

        model.addAttribute("power", power);

        return "editPower";
    }

    @RequestMapping(value = "/editPower", method = RequestMethod.POST)
    public String editPower(@ModelAttribute("power") SuperPower power) {
        
        service.updateSuperPower(power);

        return "redirect:powers";
    }  
}