package com.superherosightingsspringmvc.controller;

import com.superherosightingsspringmvc.dto.Location;
import com.superherosightingsspringmvc.dto.SuperSighting;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import com.superherosightingsspringmvc.service.ServiceLayer;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author brian russick
 */
@Controller
public class SightingController {
    
    ServiceLayer service;

    @Inject
    public SightingController(ServiceLayer service) {
        this.service = service;
    }

    @RequestMapping(value = "/sightings", method = RequestMethod.GET)
    public String displaySightingsPage(Model model) {

        List<SuperSighting> sightingList = service.getSuperSightingsAll();
        
        List<SuperHeroSuperVill> superList = service.getSuperHeroSuperVillAll();
        
        List<Location> locationList = service.getLocationAll();

        model.addAttribute("sightingList", sightingList);
        
        model.addAttribute("superList", superList);
        
        model.addAttribute("locationList", locationList);

        return "sightings";
    }

    @RequestMapping(value = "/createSighting", method = RequestMethod.POST)
    public String createSighting(HttpServletRequest request, Model model, String superId, String locationId) {
        
      SuperSighting sighting = new SuperSighting();
      LocalDate date = LocalDate.parse(request.getParameter("date"));
      sighting.setDate(LocalDate.of(date.getYear(), date.getMonthValue(), date.getDayOfMonth()));
           
      int superId2 = Integer.parseInt(superId);
                 
      sighting.setSuperHVId(superId2);
      
      int locaId2 = Integer.parseInt(locationId);
                 
      sighting.setLocaId(locaId2);

      service.addSuperSighting(sighting);

      return "redirect:sightings";
      
    }

    @RequestMapping(value = "/sightingDetails", method = RequestMethod.GET)
    public String displaySightingDetails(HttpServletRequest request, Model model) {
        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);

        SuperSighting sighting = service.getSuperSighting(sightingId);
        
        int superHVId = sighting.getSuperHVId();
        
        SuperHeroSuperVill superPerson = service.getSuperHeroSuperVill(superHVId);
        
        int locaId = sighting.getLocaId();
        
        Location loca = service.getLocation(locaId);

        model.addAttribute("sighting", sighting);
        
        model.addAttribute("superPerson", superPerson);
        
        model.addAttribute("location", loca);

        return "sightingDetails";
    }

    @RequestMapping(value = "/deleteSighting", method = RequestMethod.GET)
    public String deleteSighting(HttpServletRequest request) {
        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);

        service.removeSuperSighting(sightingId);

        return "redirect:sightings";
    }

    @RequestMapping(value = "/displayEditSighting", method = RequestMethod.GET)
    public String displayEditSighting(HttpServletRequest request, Model model) {
        
        String sightingIdParameter = request.getParameter("sightingId");

        int sightingId = Integer.parseInt(sightingIdParameter);

        SuperSighting sighting = service.getSuperSighting(sightingId);
        
        List<SuperHeroSuperVill> superList = service.getSuperHeroSuperVillAll();
        
        List<Location> locationList = service.getLocationAll();

        model.addAttribute("sighting", sighting);
        
        model.addAttribute("superList", superList);
        
        model.addAttribute("locationList", locationList);

        return "editSighting";
    }

    @RequestMapping(value = "/editSighting", method = RequestMethod.POST)
    public String editSighting(@ModelAttribute("sighting") SuperSighting sighting) {
        
        service.updateSuperSighting(sighting);

        return "redirect:sightings";
    }
}