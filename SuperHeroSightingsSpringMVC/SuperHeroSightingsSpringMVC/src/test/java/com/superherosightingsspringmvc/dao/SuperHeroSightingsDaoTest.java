package com.superherosightingsspringmvc.dao;

import com.superherosightingsspringmvc.dto.Location;
import com.superherosightingsspringmvc.dto.Org;
import com.superherosightingsspringmvc.dto.SuperPower;
import com.superherosightingsspringmvc.dto.SuperSighting;
import com.superherosightingsspringmvc.dto.SuperHeroSuperVill;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author brian russick
 */
public class SuperHeroSightingsDaoTest {
    
    SuperHeroSightingsDao dao;
    int locaId;
    int superHVId;
    int sightingId;
    int superPowerId;
    int orgId;
    
    public SuperHeroSightingsDaoTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        
        dao = ctx.getBean("SuperHeroSightingsDao", SuperHeroSightingsDao.class);
        
        dao.resetDatabase();
        
        Location loca = new Location();
        loca.setName("Park");
        loca.setDescription("A Park");
        loca.setAddress("123 Park Ave");
        loca.setLatitude(1.1);
        loca.setLongitude(2.2);
        dao.addLocation(loca);
        locaId = loca.getId();
        
        SuperPower power = new SuperPower();
        power.setName("Super Strength");
        power.setDescription("Really Strong");
        List<Integer> superHVIdList1 = new ArrayList<>();
        SuperHeroSuperVill superTest1 = new SuperHeroSuperVill();
        superTest1.setName("SuperMan");
        superTest1.setDescription("SuperHero");
        dao.addSuper(superTest1);
        superHVId = superTest1.getId();
        superHVIdList1.add(superHVId);
        power.setSuperHVIdList(superHVIdList1);
        dao.addSuperPower(power);
        superPowerId = power.getId();
        dao.removeSuperHeroSuperVill(superHVId);
        
        Org org = new Org();
        org.setName("X-Men");
        org.setDescription("Mutants");
        org.setAddress("123 X-Men Way");
        org.setPhoneNumber("555-5555");
        List<Integer> superHVIdList2 = new ArrayList<>();
        SuperHeroSuperVill superTest2 = new SuperHeroSuperVill();
        superTest2.setName("SuperMan");
        superTest2.setDescription("SuperHero");
        dao.addSuper(superTest2);
        superHVId = superTest2.getId();
        superHVIdList2.add(superHVId);
        org.setSuperHVIdList(superHVIdList2);
        dao.addOrg(org);
        orgId = org.getId();
        dao.removeSuperHeroSuperVill(superHVId);
        
        SuperHeroSuperVill superPerson = new SuperHeroSuperVill();
        superPerson.setName("SuperMan");
        superPerson.setDescription("SuperHero");
        List<Integer> orgIdList = new ArrayList<>();
        List<Integer> superPowerIdList = new ArrayList<>();
        orgIdList.add(orgId);
        superPowerIdList.add(superPowerId);
        superPerson.setOrgIdList(orgIdList);
        superPerson.setSuperPowerIdList(superPowerIdList);
        dao.addSuper(superPerson);
        superHVId = superPerson.getId();
    
        SuperSighting sighting = new SuperSighting();
        sighting.setDate(LocalDate.of(2010, 01, 01));
        sighting.setLocaId(locaId);
        sighting.setSuperHVId(superHVId);
        dao.addSuperSighting(sighting);
        sightingId = sighting.getId();
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSuperSighting method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetSuperSighting() {        
        
        SuperSighting test = dao.getSuperSighting(sightingId);
        
        assertEquals(java.time.LocalDate.parse("2010-01-01"), test.getDate());
        assertEquals(superHVId, test.getSuperHVId());
        assertEquals(locaId, test.getLocaId());
    
    }

    /**
     * Test of getSuperSightingsAll method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetSuperSightingsAll() {
        
        List<SuperSighting> sightingList = dao.getSuperSightingsAll();
        
        assertEquals(1, sightingList.size());
        
        SuperSighting test = sightingList.get(0);
        
        assertEquals(java.time.LocalDate.parse("2010-01-01"), test.getDate());
        assertEquals(superHVId, test.getSuperHVId());
        assertEquals(locaId, test.getLocaId());
    }

    /**
     * Test of updateSuperSighting method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testUpdateSuperSighting() {
        
        SuperSighting test = dao.getSuperSighting(sightingId);
        
        test.setDate(LocalDate.of(2011, 01, 01));
        
        dao.updateSuperSighting(test);
        
        test = dao.getSuperSighting(sightingId);
        
        LocalDate testDate = LocalDate.of(2011, 01, 01);
        
        assertEquals(testDate, test.getDate()); 
        assertEquals(superHVId, test.getSuperHVId());
        assertEquals(locaId, test.getLocaId());
   
    }

    /**
     * Test of removeSuperSighting method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testRemoveSuperSighting() {
        
        dao.removeSuperSighting(sightingId);
        
        SuperSighting test = dao.getSuperSighting(sightingId);
        
        assertNull(test);
    }


    /**
     * Test of addSuperHeroSuperVill method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testAddGetSuperHeroSuperVill() {
        SuperHeroSuperVill test = dao.getSuperHeroSuperVill(superHVId);
        
        assertEquals("SuperMan", test.getName());
        assertEquals("SuperHero", test.getDescription());
        assertEquals(1, test.getOrgIdList().size());
        assertEquals(1, test.getSuperPowerIdList().size());
        
        superPowerId = test.getSuperPowerIdList().get(0);
        SuperPower power = dao.getSuperPower(superPowerId);
        assertEquals("Super Strength", power.getName());
        assertEquals("Really Strong", power.getDescription());
        
        orgId = test.getOrgIdList().get(0);
        Org org = dao.getOrg(orgId);
        assertEquals("X-Men", org.getName());
        assertEquals("Mutants", org.getDescription());
        assertEquals("123 X-Men Way", org.getAddress());
        assertEquals("555-5555", org.getPhoneNumber());
    }

    /**
     * Test of getSuperHeroSuperVillAll method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetSuperHeroSuperVillAll() {
        List<SuperHeroSuperVill> superList = dao.getSuperHeroSuperVillAll();
        
        assertEquals(1, superList.size());
        
        SuperHeroSuperVill test = superList.get(0);
        
        assertEquals("SuperMan", test.getName());
        assertEquals("SuperHero", test.getDescription());
        assertEquals(1, test.getOrgIdList().size());
        assertEquals(1, test.getSuperPowerIdList().size()); 
        
        superPowerId = test.getSuperPowerIdList().get(0);
        SuperPower power = dao.getSuperPower(superPowerId);
        assertEquals("Super Strength", power.getName());
        assertEquals("Really Strong", power.getDescription());
        
        orgId = test.getOrgIdList().get(0);
        Org org = dao.getOrg(orgId);
        assertEquals("X-Men", org.getName());
        assertEquals("Mutants", org.getDescription());
        assertEquals("123 X-Men Way", org.getAddress());
        assertEquals("555-5555", org.getPhoneNumber());      
    }

    /**
     * Test of updateSuperHeroSuperVill method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testUpdateSuperHeroSuperVill() {
        SuperHeroSuperVill test = dao.getSuperHeroSuperVill(superHVId);
        List<Integer> orgIdList = test.getOrgIdList();
        List<Integer> superPowerIdList = test.getSuperPowerIdList();
        
        SuperPower power = new SuperPower();
        power.setName("Flight");
        power.setDescription("Flying");
        dao.addSuperPower(power);
        superPowerId = power.getId();
        superPowerIdList.add(superPowerId);
        
        Org org = new Org();
        org.setName("League of Legends");
        org.setDescription("Heros");
        org.setAddress("123 Hero Way");
        org.setPhoneNumber("111-1111");
        dao.addOrg(org);
        orgId = org.getId();
        orgIdList.add(orgId);
        
        test.setName("Hulk");
        test.setDescription("Big Green Guy");
        test.setOrgIdList(orgIdList);
        test.setSuperPowerIdList(superPowerIdList);
        
        dao.updateSuperHeroSuperVill(test);
        
        test = dao.getSuperHeroSuperVill(superHVId);
        
        assertEquals("Hulk", test.getName());
        assertEquals("Big Green Guy", test.getDescription());
        assertEquals(2, test.getOrgIdList().size());
        assertEquals(2, test.getSuperPowerIdList().size());
    }

    /**
     * Test of removeSuperHeroSuperVill method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testRemoveSuperHeroSuperVill() {
        dao.removeSuperHeroSuperVill(superHVId);
        
        SuperHeroSuperVill test = dao.getSuperHeroSuperVill(superHVId);
        
        assertNull(test);
    }
    

    /**
     * Test of addSuperPower method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testAddGetSuperPower() {
        SuperPower test = dao.getSuperPower(superPowerId);
        
        assertEquals("Super Strength", test.getName());
        assertEquals("Really Strong", test.getDescription());
        assertEquals(1, test.getSuperHVIdList().size());
        
        superHVId = test.getSuperHVIdList().get(0);
        SuperHeroSuperVill superPerson = dao.getSuperHeroSuperVill(superHVId);
        assertEquals("SuperMan", superPerson.getName());
        assertEquals("SuperHero", superPerson.getDescription());
    }

    /**
     * Test of getSuperPowerAll method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetSuperPowerAll() {
        List<SuperPower> powerList = dao.getSuperPowerAll();
        
        assertEquals(1, powerList.size());
        
        SuperPower power = powerList.get(0);
        
        assertEquals("Super Strength", power.getName());
        assertEquals("Really Strong", power.getDescription());
        assertEquals(1, power.getSuperHVIdList().size());
        
        superHVId = power.getSuperHVIdList().get(0);
        SuperHeroSuperVill superPerson = dao.getSuperHeroSuperVill(superHVId);
        assertEquals("SuperMan", superPerson.getName());
        assertEquals("SuperHero", superPerson.getDescription());
    }

    /**
     * Test of updatePower method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testUpdateSuperPower() {
        SuperPower test = dao.getSuperPower(superPowerId);
        
        List<Integer> superHVIdList = new ArrayList<>();
        SuperHeroSuperVill superPerson = new SuperHeroSuperVill();
        superPerson.setName("Hulk");
        superPerson.setDescription("Big Green Guy");
        dao.addSuper(superPerson);
        superHVId = superPerson.getId();
        superHVIdList.add(superHVId);
        
        test.setName("Flight");
        test.setDescription("Flying");
        test.setSuperHVIdList(superHVIdList);
        
        dao.updateSuperPower(test);
        
        test = dao.getSuperPower(superPowerId);
        
        assertEquals("Flight", test.getName());
        assertEquals("Flying", test.getDescription());
        assertEquals(1, test.getSuperHVIdList().size());
        
    }

    /**
     * Test of removeSuperPower method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testRemoveSuperPower() {
        dao.removeSuperPower(superPowerId);
        
        SuperPower test = dao.getSuperPower(superPowerId);
        
        assertNull(test);
    }

    /**
     * Test of addLocation method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testAddGetLocation() {
        Location test = dao.getLocation(locaId);
        
        assertEquals("Park", test.getName());
        assertEquals("A Park", test.getDescription());
        assertEquals("123 Park Ave", test.getAddress());  
    }

    /**
     * Test of getLocationAll method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetLocationAll() {
     
        List<Location> locaList = dao.getLocationAll();
        
        assertEquals(1, locaList.size());
        
        Location test = locaList.get(0);
        
        assertEquals("Park", test.getName());
        assertEquals("A Park", test.getDescription());
        assertEquals("123 Park Ave", test.getAddress());  
    }

    /**
     * Test of updateLocation method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testUpdateLocation() {
        Location test = dao.getLocation(locaId);
   
        test.setName("Work");
        test.setDescription("From Window");
        test.setAddress("123 Some Street");
        test.setLatitude(1.1);
        test.setLongitude(2.2);
        
        dao.updateLocation(test);
        
        test = dao.getLocation(locaId);
        
        assertEquals("Work", test.getName());
        assertEquals("From Window", test.getDescription());
        assertEquals("123 Some Street", test.getAddress());
           
    }

    /**
     * Test of removeLocation method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testRemoveLocation() {
        
        dao.removeLocation(locaId);
        
        Location test = dao.getLocation(locaId);
        
        assertNull(test);
    }


    /**
     * Test of addOrg method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testAddGetOrg() {
        Org test = dao.getOrg(orgId);
        
        assertEquals("X-Men", test.getName());
        assertEquals("Mutants", test.getDescription());
        assertEquals("123 X-Men Way", test.getAddress());
        assertEquals("555-5555", test.getPhoneNumber());
        assertEquals(1, test.getSuperHVIdList().size());
        
        superHVId = test.getSuperHVIdList().get(0);
        SuperHeroSuperVill superPerson = dao.getSuperHeroSuperVill(superHVId);
        assertEquals("SuperMan", superPerson.getName());
        assertEquals("SuperHero", superPerson.getDescription());
    }

    /**
     * Test of getOrgAll method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testGetOrgAll() {
        List<Org> orgList = dao.getOrgAll();
        
        assertEquals(1, orgList.size());
        
        Org test = orgList.get(0);
        
        assertEquals("X-Men", test.getName());
        assertEquals("Mutants", test.getDescription());
        assertEquals("123 X-Men Way", test.getAddress());
        assertEquals("555-5555", test.getPhoneNumber());
        assertEquals(1, test.getSuperHVIdList().size());
        
        superHVId = test.getSuperHVIdList().get(0);
        SuperHeroSuperVill superPerson = dao.getSuperHeroSuperVill(superHVId);
        assertEquals("SuperMan", superPerson.getName());
        assertEquals("SuperHero", superPerson.getDescription());
    }

    /**
     * Test of updateOrg method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testUpdateOrg() {
        Org test = dao.getOrg(orgId);
        
        List<Integer> superHVIdList = new ArrayList<>();
        SuperHeroSuperVill superPerson = new SuperHeroSuperVill();
        superPerson.setName("Hulk");
        superPerson.setDescription("Big Green Guy");
        dao.addSuper(superPerson);
        superHVId = superPerson.getId();
        superHVIdList.add(superHVId);
        
        test.setName("League Of Legends");
        test.setDescription("Heros");
        test.setAddress("123 Hero Way");
        test.setPhoneNumber("111-1111");
        test.setSuperHVIdList(superHVIdList);
        
        dao.updateOrg(test);
        
        test = dao.getOrg(orgId);
        
        assertEquals("League Of Legends", test.getName());
        assertEquals("Heros", test.getDescription());
        assertEquals("123 Hero Way", test.getAddress());
        assertEquals("111-1111", test.getPhoneNumber());
        assertEquals(1, test.getSuperHVIdList().size());
    }

    /**
     * Test of removeOrg method, of class SuperHeroSightingsDao.
     */
    @Test
    public void testRemoveOrg() {
        dao.removeOrg(orgId);
        
        Org test = dao.getOrg(orgId);
        
        assertNull(test);
    }
    
}
